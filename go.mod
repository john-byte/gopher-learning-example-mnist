module gitlab.com/john-byte/gopher-learning-example-mnist

go 1.18

require (
	gitlab.com/john-byte/gopher-learning v1.0.1 // indirect
	golang.org/x/exp v0.0.0-20191002040644-a1355ae1e2c3 // indirect
	gonum.org/v1/gonum v0.12.0 // indirect
)

package mnistexample

import (
	"fmt"
	"image"
	"image/png"
	"os"

	matmod "gonum.org/v1/gonum/mat"
)

func matrixPrint(X matmod.Matrix) {
	fa := matmod.Formatted(X, matmod.Prefix(""), matmod.Squeeze())
	fmt.Printf("%v\n", fa)
}

func dataFromImage(filePath string) []float64 {
	imgFile, err := os.Open(filePath)
	defer imgFile.Close()

	if err != nil {
		fmt.Println("Cannot read file:", err)
	}

	img, err := png.Decode(imgFile)
	if err != nil {
		fmt.Println("Cannot decode file:", err)
	}

	bounds := img.Bounds()
	gray := image.NewGray(bounds)

	for x := 0; x < bounds.Max.X; x++ {
		for y := 0; y < bounds.Max.Y; y++ {
			var rgba = img.At(x, y)
			gray.Set(x, y, rgba)
		}
	}

	pixels := make([]float64, len(gray.Pix))
	for i := 0; i < len(gray.Pix); i++ {
		pixels[i] = (float64(255-gray.Pix[i]) / 255.0 * 0.999) + 0.001
	}
	return pixels
}

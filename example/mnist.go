package mnistexample

import (
	"bufio"
	"encoding/csv"
	"fmt"
	"io"
	"log"
	"math/rand"
	"os"
	"strconv"
	"time"

	activations "gitlab.com/john-byte/gopher-learning/activation-funcs"
	deltawcalcmod "gitlab.com/john-byte/gopher-learning/delta-weight-calc"
	modelmod "gitlab.com/john-byte/gopher-learning/model"
	randutils "gitlab.com/john-byte/gopher-learning/random"
	errorsutils "gitlab.com/john-byte/gopher-learning/utils/errors"

	matmod "gonum.org/v1/gonum/mat"
)

type MnistExample struct {
	model *modelmod.TModel
}

// CreateMnistExample creates a neural MnistExample with random weights
func NewMnistExample() *MnistExample {
	inputLayer := modelmod.NewInputLayer(784)

	hiddenLayerDWCalc := deltawcalcmod.NewSigmoidDeltaWeightCalc(
		200,
		784,
	)
	hiddenLayer := modelmod.NewStandardLayer(
		200,
		matmod.NewDense(
			200,
			784,
			randutils.RandomArray(
				200*784,
				float64(784),
			),
		),
		hiddenLayerDWCalc,
		activations.Sigmoid,
	)

	outputLayerDWCalc := deltawcalcmod.NewSigmoidDeltaWeightCalc(
		10,
		200,
	)
	outputLayer := modelmod.NewStandardLayer(
		10,
		matmod.NewDense(
			10,
			200,
			randutils.RandomArray(
				200*10,
				float64(200),
			)),
		outputLayerDWCalc,
		activations.Sigmoid,
	)

	model := modelmod.NewModel(
		[]modelmod.TLayer{
			inputLayer,
			hiddenLayer,
			outputLayer,
		},
		0.1,
		"data",
	)

	net := &MnistExample{
		model: model,
	}

	return net
}

func (self *MnistExample) PredictFromImage(path string) int {
	input := dataFromImage(path)
	output := self.model.Predict(input)

	matrixPrint(output)

	best := 0
	highest := 0.0

	layers := self.model.GetLayers()
	lastlayer := layers[len(layers)-1]

	for i := 0; i < lastlayer.GetDim(); i++ {
		if output.At(i, 0) > highest {
			best = i
			highest = output.At(i, 0)
		}
	}
	return best
}

func (self *MnistExample) Train() {
	rand.Seed(time.Now().UTC().UnixNano())
	t1 := time.Now()

	for epochs := 0; epochs < 5; epochs++ {
		log.Println(fmt.Sprintf("Epoch #%v begin", epochs))

		testFile, err := os.Open("mnist_dataset/mnist_train.csv")
		if err != nil {
			log.Fatal(err)
			return
		}

		r := csv.NewReader(bufio.NewReader(testFile))
		for {
			record, err := r.Read()
			if err == io.EOF {
				break
			}

			inputs := make([]float64, 784)
			for i := range inputs {
				x, _ := strconv.ParseFloat(record[i], 64)
				inputs[i] = (x / 255.0 * 0.999) + 0.001
			}

			targets := make([]float64, 10)
			for i := range targets {
				targets[i] = 0.001
			}
			x, _ := strconv.Atoi(record[0])
			targets[x] = 0.999

			self.model.Train(inputs, targets)
		}

		testFile.Close()
		log.Println(fmt.Sprintf("Epoch #%v done", epochs))
	}

	elapsed := time.Since(t1)
	fmt.Printf("\nTime taken to train: %s\n", elapsed)
}

func (self *MnistExample) Predict() {
	t1 := time.Now()
	checkFile, _ := os.Open("mnist_dataset/mnist_test.csv")
	defer checkFile.Close()

	score := 0
	r := csv.NewReader(bufio.NewReader(checkFile))
	for {
		record, err := r.Read()
		if err == io.EOF {
			break
		}

		inputs := make([]float64, 784)
		for i := range inputs {
			if i == 0 {
				inputs[i] = 1.0
			}
			x, _ := strconv.ParseFloat(record[i], 64)
			inputs[i] = (x / 255.0 * 0.999) + 0.001
		}

		outputs := self.model.Predict(inputs)
		best := 0
		highest := 0.0
		for i := 0; i < 10; i++ {
			if outputs.At(i, 0) > highest {
				best = i
				highest = outputs.At(i, 0)
			}
		}
		target, _ := strconv.Atoi(record[0])
		if best == target {
			score++
		}
	}

	elapsed := time.Since(t1)
	fmt.Printf("Time taken to check: %s\n", elapsed)
	fmt.Println("score:", score)
}

func (self *MnistExample) Save() error {
	err := self.model.Save()
	if err != nil {
		return errorsutils.LogError(
			"MnistExample",
			"Save",
			err,
		)
	}

	return nil
}

func (self *MnistExample) Load() error {
	err := self.model.Load()
	if err != nil {
		return errorsutils.LogError(
			"MnistExample",
			"Load",
			err,
		)
	}

	return nil
}

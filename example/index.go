package mnistexample

import (
	"bytes"
	"encoding/base64"
	"flag"
	"fmt"
	"image"
	"image/png"
	"os"
)

func printImage(img image.Image) {
	var buf bytes.Buffer
	png.Encode(&buf, img)

	imgBase64Str := base64.StdEncoding.EncodeToString(buf.Bytes())
	fmt.Printf("\x1b]1337;File=inline=1:%s\a\n", imgBase64Str)
}

func getImage(filePath string) image.Image {
	imgFile, err := os.Open(filePath)
	defer imgFile.Close()

	if err != nil {
		fmt.Println("Cannot read file:", err)
		return nil
	}

	img, _, err := image.Decode(imgFile)
	if err != nil {
		fmt.Println("Cannot decode file:", err)
		return nil
	}

	return img
}

func RunMnistExample() {
	mnistNet := NewMnistExample()

	mnist := flag.String("mnist", "", "Either train or predict to evaluate neural network")
	file := flag.String("file", "", "File name of 28 x 28 PNG file to evaluate")
	flag.Parse()

	switch *mnist {
	case "train":
		mnistNet.Train()
		mnistNet.Save()
	case "predict":
		mnistNet.Load()
		mnistNet.Predict()
	default:
	}

	if *file != "" {
		img := getImage(*file)
		printImage(img)

		mnistNet.Load()
		fmt.Println("prediction:", mnistNet.PredictFromImage(*file))
	}
}

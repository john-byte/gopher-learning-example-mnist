package main

import (
	mnistmod "gitlab.com/john-byte/gopher-learning-example-mnist/example"
)

func main() {
	mnistmod.RunMnistExample()
}
